import React,{createContext} from 'react'
import Header from './components/Header'
import MainSection from './components/MainSection'
import Footer from './components//Footer'

const Context = createContext(false)

export default function Principal() {
    return (
        <Context.Provider>
            <Header></Header>
        <MainSection></MainSection>
        <Footer></Footer>
        </Context.Provider>
        
    )
}
export {Context}
