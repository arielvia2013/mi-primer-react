import blanco from '../img/tenies1.jpg'
import normal from '../img/tenies2.jpg'
import negro from '../img/tenies3.png'
const product_cart = [
    {
        id : 1,
        product_name:"Caña Alta Blanco",
        decription:"tenis de paseo caña alta color blanco",
        price:350,
        thumb:blanco
    },
    {
        id : 2,
        product_name:"Normal",
        decription:"tenis de paseo normales blanco y negro",
        price:150,
        thumb:normal
    },
    {
        id : 3,
        product_name:"Caña Alta negro",
        decription:"tenis de paseo caña alta color negro",
        price:250,
        thumb:negro
    }
]
export default product_cart
