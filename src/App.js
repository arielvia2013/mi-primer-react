import './styles/App.css';
import { Routes, Route } from "react-router-dom";
import Principal from './Principal';
import Productos from './Productos';
import Contactos from './Contactos';

function App() {
  
   
  return <div>
    <Routes>
    <Route path="/" element={<Principal></Principal>}>
    </Route>
    <Route path="/Productos" element={<Productos></Productos>}>
    </Route>
    <Route path="/Contactos" element={<Contactos></Contactos>}>
    </Route>
  </Routes>
  </div>
}

export default App;
