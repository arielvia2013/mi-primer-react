import React,{createContext} from 'react'
import Header from './components/Header'
import Footer from './components//Footer'
import MainContactos from './components/MainContactos'

const Context = createContext(false)

export default function Contactos() {
    return (
        <Context.Provider>
            <Header></Header>
            <MainContactos></MainContactos>
            <Footer></Footer>
        </Context.Provider>  
    )
}
export {Context}
