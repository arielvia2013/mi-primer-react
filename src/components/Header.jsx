import React from 'react'

export default function Header() {
    return (
        <div className='content'>
            <nav>
                <div className="logo"> <a href="/">Pumba</a></div>
                <ul>
                    <li>Servicios</li>
                    <li ><a href="/Productos">Productos</a></li>
                    <li> <a href="/Contactos">Contactos</a></li>
                </ul>
                <div className="search">
                    <button className='button-login'>Login</button>
                </div>
            </nav>
        </div>
    )
}