import React from 'react'

export default function Footer() {
    return (
        <div className='footer'> 
            <div className="social">
                <p>Siguenos en..</p>
                <i className='fa fa-facebook'></i>
                <i className='fa fa-instagram'></i>
                <i className='fa fa-twitter'></i>
            </div>
            <div className="derechos">
                <p>Derechos reservados|2021</p>
            </div>
        </div>
    )
}
