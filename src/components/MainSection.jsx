import React from 'react'
import pumba from '../assets/img/pumba.png'
export default function MainSection() {
    return (
        <div className='container-section'>
            <section className="grid-2 hero">
            <div className='hero-content'>
                <h1 className="hero-title">Venta de tenis deportivos PUMBA</h1>
                <p className="hero-description">esta marca inicio en el negocio el año 2020 y esta revolucionando
                en lo que se refiere a tenis ya sea por los modelos o la calidad................</p>
                <a className='btn-ver-mas'  href='/'>Ver mas...</a>
                <a className='btn btn-contactos'  href='/Contactos'>Contactanos</a>
            </div>
            <div className='heri-image-container'>
                <img className="hero-image" src={pumba} alt="" />  
            </div>
        </section>
        </div>
    )
}
