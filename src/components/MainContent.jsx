import React from 'react'
import product from "../assets/data/Product"

const MainContent =()=>{

    const listItems=product.map((item)=>
        <div className='card' key={item.id}>
            <div className="card_img">
                <img  src={item.thumb} alt='mi imagen'/>
            </div>
            <div className="card_header">
                <h2>{item.product_name}</h2>
                <p>{item.decription}</p>
                <p className="price">{item.price} <span>$</span></p>
            </div>
        </div>
    );
    return(
        <div className="main_content">
            <div className="titulo">
                <h3>Tenis de oferta</h3>
            </div>
            <div className="items">
                {listItems}
            </div>
            
        </div>
    )
}
export default MainContent;