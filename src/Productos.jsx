import React,{createContext} from 'react'
import Header from './components/Header'
import Footer from './components//Footer'
import MainContent from './components/MainContent'

const Context = createContext(false)

export default function Productos() {
    return (
        <Context.Provider>
            <Header></Header>
            <MainContent></MainContent>
            <Footer></Footer>
        </Context.Provider>  
    )
}
export {Context}
